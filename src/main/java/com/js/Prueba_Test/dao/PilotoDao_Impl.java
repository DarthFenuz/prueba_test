/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.dao;

import com.js.Prueba_Test.model.Piloto;
import com.js.Prueba_Test.model.PilotoRequest;
import com.js.Prueba_Test.repository.PilotoRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffs
 */
@Repository
public class PilotoDao_Impl implements PilotoDao{

    @Autowired
    private PilotoRepository pilotoRepository;

    @Override
    public boolean savePiloto(Piloto pilot) {
         boolean status = false;
        try {
            pilotoRepository.save(pilot);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public List<Piloto> getPilotos() {
        return pilotoRepository.findAll();
    }

    @Override
    public boolean deletePiloto(Piloto pilot) {
        boolean status = false;
        try {
            pilotoRepository.delete(pilot);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public Optional<Piloto> getPilotoByID(int id) {
        return pilotoRepository.findById(id);
    }

    @Override
    public boolean updatePiloto(int id, PilotoRequest piloto) {
        return pilotoRepository.findById(id)
                .map(pilot -> {
                    copy(piloto, pilot);
                    return true;
                })
                .orElse(false);
    }
    
     private void copy(PilotoRequest pilotoRequest, Piloto piloto) {
        piloto.setNombre(pilotoRequest.getNombre());
        piloto.setDireccion(pilotoRequest.getDireccion());
        piloto.setGastos(pilotoRequest.getGastos());
        piloto.setTelefono(piloto.getTelefono());
        piloto.setViatico(pilotoRequest.getViatico());
    }
    
}
