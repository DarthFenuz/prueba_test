/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.dao;

import com.js.Prueba_Test.model.Vehiculo;
import com.js.Prueba_Test.model.VehiculoRequest;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author jeffs
 */
public interface VehiculoDao {
    public boolean saveVehiculo(Vehiculo veh);
    public List<Vehiculo> getVehiculos();
     public boolean deleteVehiculo(Vehiculo veh);

    public Optional<Vehiculo> getVehiculoByID(int id);

    public boolean updateVehiculo(int id, VehiculoRequest veh);
}
