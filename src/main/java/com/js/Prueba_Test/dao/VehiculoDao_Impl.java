/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.dao;

import com.js.Prueba_Test.model.Piloto;
import com.js.Prueba_Test.model.Vehiculo;
import com.js.Prueba_Test.model.VehiculoRequest;
import com.js.Prueba_Test.repository.PilotoRepository;
import com.js.Prueba_Test.repository.VehiculoRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffs
 */
@Repository
public class VehiculoDao_Impl  implements VehiculoDao{

    @Autowired
    private VehiculoRepository vehiculoRepository;
    
    @Autowired
    private PilotoRepository pilotoRepository;

    
    @Override
    public boolean saveVehiculo(Vehiculo veh) {
         boolean status = false;
        try {
            vehiculoRepository.save(veh);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public List<Vehiculo> getVehiculos() {
        return vehiculoRepository.findAll();
    }

    @Override
    public boolean deleteVehiculo(Vehiculo veh) {
         boolean status = false;
        try {
            vehiculoRepository.delete(veh);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public Optional<Vehiculo> getVehiculoByID(int id) {
        return vehiculoRepository.findById(id);
    }

    @Override
    public boolean updateVehiculo(int id, VehiculoRequest veh) {
        return vehiculoRepository.findById(id)
                .map(ve -> {
                    copy(veh, ve);
                    return true;
                })
                .orElse(false);
    }
    
    private void copy(VehiculoRequest vehiculoRequest, Vehiculo veh) {
        veh.setCapacidad(vehiculoRequest.getCapacidad());
        veh.setConsumo(vehiculoRequest.getConsumo());
        veh.setCosto_depreciacion(vehiculoRequest.getCosto_depreciacion());
        veh.setDistancia(vehiculoRequest.getDistancia());
        veh.setFecha(vehiculoRequest.getFecha());
        veh.setModelo(vehiculoRequest.getModelo());
        veh.setPlaca(vehiculoRequest.getPlaca());
        veh.setTipo(vehiculoRequest.getTipo());
    }
    
}
