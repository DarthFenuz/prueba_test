/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.dao;

import com.js.Prueba_Test.model.Producto;
import com.js.Prueba_Test.repository.ProductRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffs
 */
@Repository
public class ProductoDao_Impl implements ProductoDao {

    @Autowired
    private ProductRepository productoRepository;

    
    @Override
    public boolean saveProduct(Producto prod) {
       boolean status = false;
        try {
            productoRepository.save(prod);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public List<Producto> getProducts() {
         return productoRepository.findAll();
    }
    
}
