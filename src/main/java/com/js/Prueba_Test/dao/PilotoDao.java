/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.dao;

import com.js.Prueba_Test.model.Piloto;
import com.js.Prueba_Test.model.PilotoRequest;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author jeffs
 */
public interface PilotoDao {
    public boolean savePiloto(Piloto pilot);
    public List<Piloto> getPilotos();
    public boolean deletePiloto(Piloto pilot);

    public Optional<Piloto> getPilotoByID(int id);

    public boolean updatePiloto(int id, PilotoRequest piloto);
}
