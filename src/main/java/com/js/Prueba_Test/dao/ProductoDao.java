/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.dao;

import com.js.Prueba_Test.model.Producto;
import java.util.List;

/**
 *
 * @author jeffs
 */
public interface ProductoDao {
    public boolean saveProduct(Producto student);
	public List<Producto> getProducts();
}
