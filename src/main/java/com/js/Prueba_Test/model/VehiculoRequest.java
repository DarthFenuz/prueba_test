/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.model;

/**
 *
 * @author jeffs
 */
public class VehiculoRequest {
    private String distancia;
    private String tipo;
    private String modelo;
    private float costo_depreciacion;
    private float capacidad;
    private float consumo;
    private String placa;
    private String fecha;
    private int piloto_id;

    public VehiculoRequest() {
    }

    public VehiculoRequest(String distancia, String tipo, String modelo, float costo_depreciacion, float capacidad, float consumo, String placa,
            String fecha, int piloto_id) {
        this.distancia = distancia;
        this.tipo = tipo;
        this.modelo = modelo;
        this.costo_depreciacion = costo_depreciacion;
        this.capacidad = capacidad;
        this.consumo = consumo;
        this.placa = placa;
        this.fecha = fecha;
        this.piloto_id = piloto_id;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public float getCosto_depreciacion() {
        return costo_depreciacion;
    }

    public void setCosto_depreciacion(float costo_depreciacion) {
        this.costo_depreciacion = costo_depreciacion;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getPiloto_id() {
        return piloto_id;
    }

    public void setPiloto_id(int piloto_id) {
        this.piloto_id = piloto_id;
    }
    
    

}
