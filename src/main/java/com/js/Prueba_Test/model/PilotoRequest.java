/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.model;

/**
 *
 * @author jeffs
 */
public class PilotoRequest {
  
    private String nombre;

    private String telefono;

    private String direccion;

    private float viatico;

    private float gastos;

    public PilotoRequest() {
    }

    public PilotoRequest(String nombre, String telefono, String direccion, float viatico, float gastos) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.viatico = viatico;
        this.gastos = gastos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public float getViatico() {
        return viatico;
    }

    public void setViatico(float viatico) {
        this.viatico = viatico;
    }

    public float getGastos() {
        return gastos;
    }

    public void setGastos(float gastos) {
        this.gastos = gastos;
    }
    
    
}
