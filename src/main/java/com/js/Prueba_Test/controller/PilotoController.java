/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.controller;

import com.js.Prueba_Test.model.Piloto;
import com.js.Prueba_Test.model.PilotoRequest;
import com.js.Prueba_Test.service.PilotoService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeffs
 */
@RestController
@RequestMapping(value="/api/v1")
public class PilotoController {
    @Autowired
    private PilotoService pilotoService;
    
    @PostMapping("/guardarPiloto")
    public boolean savePiloto(@RequestBody Piloto pilot) {
        return pilotoService.savePiloto(pilot);

    }

    @GetMapping("/listaPilotos")
    public List<Piloto> allPilotos() {
        return pilotoService.getPilotos();
    }
 
    @DeleteMapping("deletePiloto/{piloto_id}")
    public boolean deletePiloto(@PathVariable("piloto_id") int piloto_id, Piloto piloto) {
        piloto.setId(piloto_id);
        return pilotoService.deletePiloto(piloto);
    }
  
    @GetMapping("piloto/{piloto_id}")
    public Optional<Piloto> allPilotoByID(@PathVariable("piloto_id") int piloto_id, Piloto piloto) {
        return pilotoService.getPilotoByID(piloto_id);

    }

    @PutMapping("updatePiloto/{piloto_id}")
    public boolean updatePiloto(@RequestBody PilotoRequest piloto, @PathVariable("piloto_id") int piloto_id) {
        return pilotoService.updatePiloto(piloto_id, piloto);
    }
}
