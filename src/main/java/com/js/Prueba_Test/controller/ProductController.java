/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.controller;

import com.js.Prueba_Test.model.Producto;
import com.js.Prueba_Test.service.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeffs
 */
@RestController
@RequestMapping(value="/api")
public class ProductController {

    @Autowired
    private ProductoService productService;
    
    @RequestMapping("/")
    public String index() {
        return "Greetings!";
    }
    
    @PostMapping("/guardarProducto")
    public boolean saveProduct(@RequestBody Producto prod) {
        return productService.saveProduct(prod);

    }

    @GetMapping("/listaProductos")
    public List<Producto> allProduct() {
        return productService.getProductos();
    }
    
   
}
