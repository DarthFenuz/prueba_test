/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.controller;

import com.js.Prueba_Test.model.Vehiculo;
import com.js.Prueba_Test.model.VehiculoRequest;
import com.js.Prueba_Test.service.VehiculoService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeffs
 */
@RestController
@RequestMapping(value="/api/v1")
public class VehiculoController {
    @RequestMapping("/")
    public String index() {
        return "Greetings!";
    }
   
    @Autowired
    private VehiculoService vehiculoService;
   
    @PostMapping("/guardarVehiculo")
    public boolean saveVehiculo(@RequestBody Vehiculo veh) {
        return vehiculoService.saveVehiculo(veh);

    }

    @GetMapping("/listaVehiculos")
    public List<Vehiculo> allVehiculos() {
        return vehiculoService.getVehiculos();
    }
    
    @DeleteMapping("deleteVehiculo/{veh_id}")
    public boolean deleteVehiculo(@PathVariable("veh_id") int veh_id, Vehiculo veh) {
        veh.setId_ve(veh_id);
        return vehiculoService.deleteVehiculo(veh);
    }
  
    @GetMapping("Vehiculo/{veh_id}")
    public Optional<Vehiculo> allVehiculoByID(@PathVariable("veh_id") int veh_id, Vehiculo piloto) {
        return vehiculoService.getVehiculoByID(veh_id);

    }

    @PutMapping("updateVehiculo/{veh_id}")
    public boolean updateVehiculo(@RequestBody VehiculoRequest veh, @PathVariable("veh_id") int veh_id) {
        return vehiculoService.updateVehiculo(veh_id, veh);
    }
}
