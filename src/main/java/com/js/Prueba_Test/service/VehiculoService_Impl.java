/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.service;

import com.js.Prueba_Test.dao.VehiculoDao;
import com.js.Prueba_Test.model.Vehiculo;
import com.js.Prueba_Test.model.VehiculoRequest;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jeffs
 */
@Service
public class VehiculoService_Impl implements VehiculoService{
    
    @Autowired
    private VehiculoDao vehiculodao;
    
    @Override
    public boolean saveVehiculo(Vehiculo veh) {
        return vehiculodao.saveVehiculo(veh);
    }

    @Override
    public List<Vehiculo> getVehiculos() {
        return vehiculodao.getVehiculos();
    }

    @Override
    public boolean deleteVehiculo(Vehiculo veh) {
        return vehiculodao.deleteVehiculo(veh);
    }

    @Override
    public Optional<Vehiculo> getVehiculoByID(int id) {
       return vehiculodao.getVehiculoByID(id);
    }

    @Override
    @Transactional
    public boolean updateVehiculo(int id, VehiculoRequest veh) {
        return vehiculodao.updateVehiculo(id, veh);
    }
}
