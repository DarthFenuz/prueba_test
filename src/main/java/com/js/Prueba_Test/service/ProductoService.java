/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.service;

import com.js.Prueba_Test.model.Producto;
import java.util.List;

/**
 *
 * @author jeffs
 */
public interface ProductoService {
    public boolean saveProduct(Producto prod);
    public List<Producto> getProductos();
}
