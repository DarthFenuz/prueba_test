/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.service;

import com.js.Prueba_Test.model.Piloto;
import com.js.Prueba_Test.model.PilotoRequest;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author jeffs
 */
public interface PilotoService {
    public boolean savePiloto(Piloto prod);
    public List<Piloto> getPilotos();
    public boolean deletePiloto(Piloto piloto);

    public Optional<Piloto> getPilotoByID(int id);

    public boolean updatePiloto(int id, PilotoRequest piloto);
}
