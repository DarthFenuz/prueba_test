/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.service;

import com.js.Prueba_Test.dao.PilotoDao;
import com.js.Prueba_Test.model.Piloto;
import com.js.Prueba_Test.model.PilotoRequest;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jeffs
 */
@Service
public class PilotoService_Impl implements PilotoService{

     @Autowired
    private PilotoDao pilotodao;
    
    @Override
    public boolean savePiloto(Piloto pilot) {
        return pilotodao.savePiloto(pilot);
    }

    @Override
    public List<Piloto> getPilotos() {
        return pilotodao.getPilotos();
    }

    @Override
    public boolean deletePiloto(Piloto piloto) {
        return pilotodao.deletePiloto(piloto);
    }

    @Override
    public Optional<Piloto> getPilotoByID(int id) {
        return pilotodao.getPilotoByID(id);
    }

    @Override
    @Transactional
    public boolean updatePiloto(int id, PilotoRequest piloto) {
        return pilotodao.updatePiloto(id, piloto);
    }

}
