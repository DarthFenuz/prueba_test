/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.service;

import com.js.Prueba_Test.dao.ProductoDao;
import com.js.Prueba_Test.model.Producto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeffs
 */
@Service
public class ProductService_Impl implements ProductoService{

     @Autowired
    private ProductoDao productdao;

    
    @Override
    public boolean saveProduct(Producto prod) {
        
         return   productdao.saveProduct(prod);
         
    }

    @Override
    public List<Producto> getProductos() {
        return productdao.getProducts();
    }
    
}
