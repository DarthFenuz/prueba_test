/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.js.Prueba_Test.repository;

import com.js.Prueba_Test.model.Vehiculo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jeffs
 */
public interface VehiculoRepository extends JpaRepository<Vehiculo, Integer>{
    
}
